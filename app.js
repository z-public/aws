const express = require("express");
const Presign = require("./lib/presign");
const mime = require("mime");
const app = express();

app.set("view engine", "ejs");
//app.use(express.urlencoded({ extended: false }))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.post("/create-resigned-post", async (req, res) => {
  req.on("data", async chunk => {
    if (JSON.parse(chunk) == null) {
      return res.sendStatus(404);
    } else {
      try {
        const { name, type, id } = JSON.parse(chunk);

        if (name === "" || id === "") return res.sendStatus(404);

        const presignedPostData = await Presign.createPresignedPost({
          key: `${name}`,
          contentType: mime.getType(name),
          type,
          id
        });

        return res.status(200).send(presignedPostData);
      } catch (err) {
        return res.sendStatus(404);
      }
    }
  });
});

app.post("/get-presigned-url", async (req, res) => {
  req.on("data", chunk => {
    if (JSON.parse(chunk) == null) return res.sendStatus(404);

    try {
      const { name, id, type } = JSON.parse(chunk);

      const url = Presign.getPresignedImageUrl({ key: name, id, type });

      if (!url) return res.sendStatus(404);

      res.status(200).json({ url });
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  });
});

app.listen(process.env.PORT || 5000);
