const AWS = require("aws-sdk");
const config = require("../config");

const s3 = new AWS.S3();

AWS.config.update({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
  region: config.region,
  bucket: config.bucket
});

function getParent(type) {
  switch (type) {
    case 0:
      return "folder1";
    case 1:
      return "folder2";
    default:
      return "folder1";
  }
}

exports.createPresignedPost = ({ key, contentType, type, id }) => {
  const params = {
    Expires: 60,
    Bucket: `${AWS.config.bucket}`,
    Conditions: [["content-length-range", 100, 10000000]], // 100Byte - 10MB
    Fields: {
      "Content-Type": contentType,
      key: `${getParent(type)}/${id}/${key}`
    }
  };

  return new Promise(async (resolve, reject) => {
    s3.createPresignedPost(params, function(err, data) {
      if (err) {
        console.log(err);
        reject(err);
      }
      resolve(data);
    });
  });
};

exports.getPresignedImageUrl = ({ key, id, type }) => {
  const params = {
    Bucket: `${AWS.config.bucket}/${getParent(type)}/${id}`,
    Key: key,
    Expires: 60
    //ContentType: contentType
  };

  const url = s3.getSignedUrl("getObject", params);

  return url;
};
