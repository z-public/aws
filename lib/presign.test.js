const sampleInput = {
  lastModified: 1590049428000,
  lastModifiedDate: "Thu May 21 2020 16:23:48 GMT+0800 (Malaysia Time)",
  name: "DC-promo06.jpg",
  size: 135005,
  type: "image/jpeg",
  webkitRelativePath: ""
};

const URL = "http://192.168.43.131:5000/presignedUrl";

const body = JSON.stringify({
  name: sampleInput.name,
  type: sampleInput.type
});

fetch(URL, { method: "POST", body })
  .then(res => res.json())
  .then(res => console.log(res));
